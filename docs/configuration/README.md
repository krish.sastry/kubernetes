# Configuration

## Table of Contents

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Configuration](#configuration)
  - [Table of Contents](#table-of-contents)
  - [Install Helm](#install-helm)
  - [Install NFS-Client](#install-nfs-client)
  - [Install MetalLB](#install-metallb)
  - [Install Consul](#install-consul)
  - [~~Install Traefik~~](#install-traefik)
  - [Install Nginx-Ingress](#install-nginx-ingress)
  - [Install Cert-Manager](#install-cert-manager)
    - [Create Production Issuer](#create-production-issuer)
    - [Create Cloudflare API Key Secret](#create-cloudflare-api-key-secret)
    - [Create Default Certificate](#create-default-certificate)
    - [Create ingress for Consul and Dashboard](#create-ingress-for-consul-and-dashboard)
  - [Install Prometheus-Operator](#install-prometheus-operator)
  - [Install Weave-Scope](#install-weave-scope)
  - [Bank-Vaults](#bank-vaults)
  - [Next Steps](#next-steps)

<!-- /code_chunk_output -->

## Install Helm

**NOTE:** If using the supplied inventory, Helm is automatically installed via [addons.md](../../src/installation/inventory/Justin-Tech/group_vars/k8s-cluster/addons.yml).

1. Download Helm
   * copy [rbac-config.yaml](../../src/configuration/rbac-config.yaml) into the working directory

2. Run `kubectl create -f rbac-config.yaml` to create a tiller service account with cluster-admin role

3. Run `helm init --service-account tiller` to install helm with the above service account

## Install NFS-Client

1. Install NFS-Client to use NFS share for persistent storage

   * `helm install stable/nfs-client-provisioner --name nfs-client --namespace kube-system --set nfs.server=10.0.40.5 --set nfs.path=/mnt/Shared/kubernetes`

2. Get the status of nfs-client with `helm status nfs-client`

## Install MetalLB

1. Review the Helm Chart and associated documentation found [here](https://github.com/helm/charts/tree/master/stable/metallb).

2. Apply any changes to [values.yaml](../../src/configuration/metallb/values.yaml)

3. Deploy the Helm Chart (changing the path of values.yaml to the location of the repository)

   * `helm install --name metallb -f values.yaml stable/metallb`

## Install Consul

**NOTE:** Consul will be depreciated in a future version and replaced by [Istio](https://istio.io/)

1. Clone the consul-helm repository found [here](https://github.com/hashicorp/consul-helm)

   * `git clone https://github.com/hashicorp/consul-helm.git`

2. Change to the consul-helm directory.

   * `cd consul-helm`

3. Checkout the latest tag (we will be using v0.5.0)

   * `git checkout v0.5.0`

4. Make any changes to [values.yaml](../../src/configuration/consul/values.yaml) as needed

   * Currently setup to expose the UI on a NodePort change to ClusterIP when using an ingress

5. Perform a dry-run of the Consul installation

   * `helm install --dry-run ./`

6. Install Consul by running

   * `helm install -n consul ./`

## ~~Install Traefik~~

**NOTE:** Deprecated in favour of [Nginx-Ingress](#install-nginx-ingress)

**NOTE:** Does not currently use Consul as a provider

1. Make any changes to [values.yaml](../../src/configuration/traefik/values.yaml)

2. Install Traefik
   * `helm install stable/traefik --name traefik --namespace kube-system --values values.yaml`

**NOTES:**

* Traefik helm chart does not support resolvers
* Traefik (lego) requires SOA records to issue domains
* Have to point the DNS servers to external (eg. 1.1.1.1) for *.corp.justin-tech.com wildcards (see [coredns configmap](../../src/configuration/coredns))

## Install Nginx-Ingress

1. Review documentation for [nginx-ingress](https://github.com/helm/charts/tree/master/stable/nginx-ingress)

2. Make any changes to [values.yaml](../../src/configuration/nginx-ingress/values.yaml)

3. Deploy the Helm Chart

   * `helm install --name nginx-ingress -f values.yaml stable/nginx-ingress --namespace nginx-ingress`

## Install Cert-Manager

1. Review documentation for [cert-manager](https://github.com/jetstack/cert-manager/tree/master/deploy/charts/cert-manager)

2. Install the CustomResourceDefinition resources separately

   * `kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.7/deploy/manifests/00-crds.yaml`

3. Create the namespace for cert-manager

   * `kubectl create namespace cert-manager`

4. Label the cert-manager namespace to disable resource validation

   * `kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true`

5. Add the Jetstack Helm repository

   * `helm repo add jetstack https://charts.jetstack.io`

6. Update your local Helm chart repository cache

   * `helm repo update`

7. Make any changes to [values.yaml](../../src/configuration/cert-manager/values.yaml)

8. Deploy the Helm Chart

   * `helm install --name cert-manager --namespace cert-manager -f values.yaml jetstack/cert-manager`

9. [Verify the installation](https://docs.cert-manager.io/en/latest/getting-started/install.html#verifying-the-installation)

### Create Production Issuer

* ~~**NOTE:** Helm-Vault does not currently support the nested dictionary found in letsencrypt.yaml~~ This has been resolved in Helm-Vault v0.1.1

1. Apply [letsencrypt.yaml](../../src/configuration/cert-manager/letsencrypt.yaml)

   * `kubectl apply -f letsencrypt.yaml`

### Create Cloudflare API Key Secret

1. Apply [cloudflare-secret.yaml](../../src/configuration/cert-manager/cloudflare-secret.yaml)

   * `kubectl apply -f cloudflare-secret.yaml`

### Create Default Certificate

1. Apply [certificate.yaml](../../src/configuration/cert-manager/certificate.yaml)

   * `kubectl apply -f certificate.yaml`

### Create ingress for Consul and Dashboard

1. Apply ingress.yaml files ([Consul](../../src/configuration/consul/ingress.yaml) and [Dashboard](../../src/configuration/dashboard/ingress.yaml))

   * `kubectl apply -f ingress.yaml`

2. The default locations are at [https://consul.corp.justin-tech.com](https://consul.corp.justin-tech.com) and [https://kubernetes.corp.justin-tech.com](https://kubernetes.corp.justin-tech.com)

## Install Prometheus-Operator

**NOTE:** There is example source for the prometheus helm chart at [Prometheus](../../src/configuration/prometheus/values.yaml), however we will be using the prometheus-operator installation

1. Review the documentation and values.yaml of [prometheus-operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator)

2. Make any needed changes to [values.yaml](../../src/configuration/prometheus-operator/values.yaml)

3. Install prometheus-operator

    * `helm install prometheus stable/prometheus-operator -f values.yaml --namespace monitoring`

## Install Weave-Scope

**NOTE:** This is an optional install, [Istio](https://istio.io) and [Kiali](https://www.kiali.io/) will be preferred in future versions

1. Review documentation and values.yaml of [weave-scope](https://github.com/helm/charts/tree/master/stable/weave-scope)

2. Make any needed changes to [values.yaml](../../src/configuration/weave-scope/values.yaml)

3. Install weave-scope

    * `helm install weave-scope stable/weave-scope -f values.yaml --namespace monitoring`

## Bank-Vaults

[Walkthrough](https://medium.com/@jackalus/deploying-vault-with-etcd-backend-in-kubernetes-d89f9a0068bf)

1. Deploy the storage backend, we are using Consul

2. Add the BanzaiCloud Helm repo

3. Install the operator

4. `kubectl apply -f https://raw.githubusercontent.com/banzaicloud/bank-vaults/master/operator/deploy/rbac.yaml`

5. Apply `vault-cluster.yaml`
   1. Once setup, setup ldap/oidc users, ect

6. Apply `clusterrole.yaml`

7. Follow steps from https://medium.com/@jackalus/deploying-vault-with-etcd-backend-in-kubernetes-d89f9a0068bf > Let's get in

8. Deploy the webhook
   1. `kubectl create ns vswh`
   2. helm install with `values.yaml`

9. Setup test secrets
   1.  `vault kv put secret/accounts/aws AWS_SECRET_ACCESS_KEY=s3cr3t`

10. Deploy test app
    1.  `kubectl apply -f test-deployment.yaml`

**NOTE:** To get all metrics, statsd seems to be required. Direct prometheus stats are limited.

## Next Steps

* ~~Look at setting up OAuth (SAML/OIDC) authentication for Kubernetes services (I suspect this may require Nginx Ingress)~~ (See #39 - implemented in [Oauth2-Proxy](../../src/services/oauth2-proxy))
* ~~Setup MetalLB for load balancing (would allow Traefik/Nginx to run on standard ports)~~ (See [Install MetalLB](#install-metallb))
* Configure Consul Connect to secure communications between services